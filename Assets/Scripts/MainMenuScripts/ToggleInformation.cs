﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleInformation : MonoBehaviour {

	public GameInformation.GameModes mode;
	public GameInformation.GameModifier modifier;
	public GameInformation.SoloII soloIIDifficulty;

}
